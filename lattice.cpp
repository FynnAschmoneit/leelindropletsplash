// lattice.cpp

#include "lattice.h"
#include <iostream>
#include "math.h"
#include "auxiliary.h"
#include <cstdio>

lattice::lattice(int width, int hight, double densH, double densL, double kap, double bet){
	lenX = width;
	lenY = hight;
	rhoH = densH;
	rhoL = densL;
	spos = 1.0/sqrt(3.0);
    invSposSq = 3.0;
    kappa = kap;
    beta = bet;
    pIn = 0.0;
    pOut = 0.0;
    nodesIn = 0;
    nodesOut = 0;
    relError = 0.0;
    startX = 2;
    startY = 2;
    endX = width +2;
    endY = hight +2;

	e = new double*[9];
    for (int i = 0; i< 9; i++){
        e[i] = new double[2];
    }
    wght = new double[9];

	e[0][0] = 0.0; 	e[0][1] = 0.0;  wght[0] = 4.0/9.0;
    e[1][0] = 1.0;  e[1][1] = 0.0;  wght[1] = 1.0/9.0;
    e[2][0] = 0.0;  e[2][1] = 1.0;  wght[2] = 1.0/9.0;
    e[3][0] = -1.0;	e[3][1] = 0.0; 	wght[3] = 1.0/9.0;
    e[4][0] = 0.0;	e[4][1] = -1.0; wght[4] = 1.0/9.0;
    e[5][0] = 1.0;  e[5][1] = 1.0;  wght[5] = 1.0/36.0;
    e[6][0] = -1.0;	e[6][1] = 1.0; 	wght[6] = 1.0/36.0;
    e[7][0] = -1.0; e[7][1] = -1.0; wght[7] = 1.0/36.0;
    e[8][0] = 1.0; 	e[8][1] = -1.0; wght[8] = 1.0/36.0;

    nd = new latticeNode*[lenX+4];
    for (int i =0; i<lenX+4; i++){
    	nd[i] = new latticeNode[lenY+4];
    }

	for(int i=startX; i<endX; i++){     //defining coordinates and nearest neighbors
		for(int j=startY; j<endY; j++){
			nd[i][j].x = i;
			nd[i][j].y = j;

            nd[i][j].nn[0] = i+1;
            nd[i][j].nn[1] = j+1;
            nd[i][j].nn[2] = i-1;
            nd[i][j].nn[3] = j-1;
            nd[i][j].nn[4] = i+2;
            nd[i][j].nn[5] = j+2;
            nd[i][j].nn[6] = i-2;
            nd[i][j].nn[7] = j-2;
		}	
	}
}


void lattice::checkLatticeForErrors(){
    double a;
    for ( int i = startX; i<endX ;i++){
        for(int j = startY; j<endY; j++){
            for( int l=0; l<9; l++){
                a = 1.0/0.0;//nd[i][j].f[l];
                if( a != a ){
                    printf("--------------------NAN in node %d %d detected!--------------\n",i,j);
                    printf("--------------------NAN in node %d %d detected!--------------\n",i,j);
                    printf("--------------------NAN in node %d %d detected!--------------\n",i,j);
                    printf("--------------------NAN in node %d %d detected!--------------\n",i,j);
                    printf("--------------------NAN in node %d %d detected!--------------\n",i,j);
                    printf("--------------------NAN in node %d %d detected!--------------\n",i,j);
                    printf("--------------------NAN in node %d %d detected!--------------\n",i,j);
                    printf("--------------------NAN in node %d %d detected!--------------\n",i,j);
                    printf("--------------------NAN in node %d %d detected!--------------\n",i,j);
                    printf("--------------------NAN in node %d %d detected!--------------\n",i,j);
                } 
            }
        }
    }
}


// copying outside nodes to outer-boundary layer. sets derivatives in outer-boundary layer to zero
void lattice::initialiseBoundaryDerivs(){

    for(int j= startY-1; j<endY+1; j++){
        for(int l = 1; l<9; l++){       // directional derivatives on Rho and b
            nd[startX-1][j].dirDerivRho[l] = 0.0;
            nd[startX-1][j].dirDerivB[l] = 0.0;
            nd[startX-1][j].dirDerivPsi[l] = 0.0;

            nd[endX][j].dirDerivRho[l] = 0.0;
            nd[endX][j].dirDerivB[l] = 0.0;
            nd[endX][j].dirDerivPsi[l] = 0.0;
        }
            // defining derivatives:
        nd[startX-1][j].drv[0] = 0.0;           // del_x
        nd[startX-1][j].drv[1] = 0.0;
        nd[startX-1][j].drv[2] = 0.0;      // del_xx
        nd[startX-1][j].drv[3] = 0.0;      
        nd[startX-1][j].drv[4] = 0.0;           // del_xy
        nd[startX-1][j].drv[5] = 0.0;

        nd[endX][j].drv[0] = 0.0;           // del_x
        nd[endX][j].drv[1] = 0.0;
        nd[endX][j].drv[2] = 0.0;      // del_xx
        nd[endX][j].drv[3] = 0.0;      
        nd[endX][j].drv[4] = 0.0;           // del_xy
        nd[endX][j].drv[5] = 0.0;
    }

    for(int i= startX; i<endX; i++){
        for(int l = 1; l<9; l++){       // directional derivatives on Rho and b
            nd[i][endY].dirDerivRho[l] = 0.0;
            nd[i][endY].dirDerivB[l] = 0.0;
            nd[i][endY].dirDerivPsi[l] = 0.0;

            nd[i][startY-1].dirDerivRho[l] = 0.0;
            nd[i][startY-1].dirDerivB[l] = 0.0;
            nd[i][startY-1].dirDerivPsi[l] = 0.0;
        }
            // defining derivatives:
        nd[i][endY].drv[0] = 0.0;           // del_x
        nd[i][endY].drv[1] = 0.0;
        nd[i][endY].drv[2] = 0.0;      // del_xx
        nd[i][endY].drv[3] = 0.0;      
        nd[i][endY].drv[4] = 0.0;           // del_xy
        nd[i][endY].drv[5] = 0.0;

        nd[i][startY-1].drv[0] = 0.0;           // del_x
        nd[i][startY-1].drv[1] = 0.0;
        nd[i][startY-1].drv[2] = 0.0;      // del_xx
        nd[i][startY-1].drv[3] = 0.0;      
        nd[i][startY-1].drv[4] = 0.0;           // del_xy
        nd[i][startY-1].drv[5] = 0.0;
    }
}

// streams outer-boundary layer values to corresp. nodes inside boundary.. to be called from preStrColl()
void lattice::copyBoundaries(){
//periodic boundary:
    // on left side:
    for(int j=startY; j<endY; j++){
        nd[startX-1][j] = nd[endX-1][j];
        nd[startX-2][j] = nd[endX-2][j];
    }

    //right side:
    for(int j=startY; j<endY; j++){
        nd[endX][j] = nd[startX][j];
        nd[endX+1][j] = nd[startX+1][j];
    }

    //top side:
    for(int i=startX; i<endX; i++){
        //nd[i][endY] = nd[i][startY];
        //nd[i][endY+1] = nd[i][startY+1];
	nd[i][endY] = nd[i][endY-1];
        nd[i][endY+1] = nd[i][endY-1];
      
    }

    //bottom side:
    for(int i=startX; i<endX; i++){
        //nd[i][startY-1] = nd[i][endY-1];
        //nd[i][startY-2] = nd[i][endY-2];
        nd[i][startY-1].rho = nd[i][startY].rho;      //hard boundary at bottom side
        nd[i][startY-2].rho = nd[i][startY].rho;
    }

    // bottom left corner
    nd[startX-1][startY-1] = nd[endX-1][endY-1]; 
    nd[startX-1][startY-2] = nd[endX-1][endY-2];
    nd[startX-2][startY-2] = nd[endX-2][endY-2];
    nd[startX-2][startY-1] = nd[endX-2][endY-1];

    // top left corner
    nd[startX-1][endY] = nd[endX-1][startY]; 
    nd[startX-1][endY+1] = nd[endX-1][startY+1]; 
    nd[startX-2][endY] = nd[endX-2][startY];
    nd[startX-2][endY+1] = nd[endX-2][startY+1];

    //top right
    nd[endX][endY] = nd[startX][startY];
    nd[endX][endY+1] = nd[startX][startY+1];
    nd[endX+1][endY] = nd[startX+1][startY];
    nd[endX+1][endY+1] = nd[startX+1][startY+1];

    // bottom right
    nd[endX][startY-1] = nd[startX][endY-1];
    nd[endX][startY-2] = nd[startX][endY-2];
    nd[endX+1][startY-1] = nd[startX+1][endY-1];
    nd[endX+1][startY-2] = nd[startX+1][endY-2];
}

//to be called after preStrCollAndStr()
void lattice::boundaryStream(){
    for( int j=startY; j<endY;j++){
        // pb from left
        nd[endX-1][j].fBar[3] = nd[startX-1][j].fBar[3];
        nd[endX-1][j].fBar[6] = nd[startX-1][j].fBar[6];
        nd[endX-1][j].fBar[7] = nd[startX-1][j].fBar[7];
        nd[endX-1][j].gBar[3] = nd[startX-1][j].gBar[3];
        nd[endX-1][j].gBar[6] = nd[startX-1][j].gBar[6];
        nd[endX-1][j].gBar[7] = nd[startX-1][j].gBar[7];

        // from right
        nd[startX][j].fBar[1] = nd[endX][j].fBar[1];
        nd[startX][j].fBar[5] = nd[endX][j].fBar[5];
        nd[startX][j].fBar[8] = nd[endX][j].fBar[8];
        nd[startX][j].gBar[1] = nd[endX][j].gBar[1];
        nd[startX][j].gBar[5] = nd[endX][j].gBar[5];
        nd[startX][j].gBar[8] = nd[endX][j].gBar[8];
    }

    for( int i=startX; i<endX; i++){
        // from top
        nd[i][startY].fBar[2] = nd[i][endY].fBar[2];
        nd[i][startY].fBar[5] = nd[i][endY].fBar[5];
        nd[i][startY].fBar[6] = nd[i][endY].fBar[6];
        nd[i][startY].gBar[2] = nd[i][endY].gBar[2];
        nd[i][startY].gBar[5] = nd[i][endY].gBar[5];
        nd[i][startY].gBar[6] = nd[i][endY].gBar[6];

        // from bottom
        nd[i][endY-1].fBar[4] = nd[i][startY-1].fBar[4];
        nd[i][endY-1].fBar[7] = nd[i][startY-1].fBar[7];
        nd[i][endY-1].fBar[8] = nd[i][startY-1].fBar[8];
        nd[i][endY-1].gBar[4] = nd[i][startY-1].gBar[4];
        nd[i][endY-1].gBar[7] = nd[i][startY-1].gBar[7];
        nd[i][endY-1].gBar[8] = nd[i][startY-1].gBar[8];
    }

/*
    for( int i=startX; i<endX; i++){
        nd[i][startY].fBar[2] = nd[i][startY].fBar[4];
        nd[i][startY].gBar[2] = nd[i][startY].gBar[4];
        nd[i][startY].fBar[5] = nd[i][startY].fBar[7];
        nd[i][startY].gBar[5] = nd[i][startY].gBar[7];
        nd[i][startY].fBar[6] = nd[i][startY].fBar[8];
        nd[i][startY].gBar[6] = nd[i][startY].gBar[8];

    }
    */
    // corners
    nd[endX-1][startY].fBar[6] = nd[startX-1][endY].fBar[6];
    nd[endX-1][startY].gBar[6] = nd[startX-1][endY].gBar[6];

    nd[endX-1][endY-1].fBar[7] = nd[startX-1][startY-1].fBar[7];
    nd[endX-1][endY-1].gBar[7] = nd[startX-1][startY-1].gBar[7];

    nd[startX][endY-1].fBar[8] = nd[endX][startY-1].fBar[8];
    nd[startX][endY-1].gBar[8] = nd[endX][startY-1].gBar[8];

    nd[startX][startY].fBar[5] = nd[endX][endY].fBar[5];
    nd[startX][startY].gBar[5] = nd[endX][endY].gBar[5];

}


void lattice::setBar(int length, int width){
    double smAmpl;
    for(int j=0; j< length + width; j++){
            smAmpl = 0.5*(rhoH+rhoL) + 0.5*(rhoH-rhoL)*tanh(2.0/width * (  width - j ));    
        for ( int i = 0; i<lenX ;i++){
            for(int l=0; l<9;l++){
                nd[i][j].f[l] = wght[l]*smAmpl;
            }    
            nd[i][j].rho = smAmpl;
        }
    }
}

void lattice::setBubble(int midX, int midY, int rad, int width){
    double smAmpl;
    double dstc;
    for ( int i = startX; i<endX ;i++){
        for(int j = startY; j<endY; j++){
            dstc = sqrt( p2(double(i-midX)) + p2(double(j-midY)) );
            smAmpl = 0.5*(rhoH+rhoL) + 0.5*(rhoH-rhoL)*tanh( 2.0/double(width) * ( rad - dstc)) ;   
            for(int l=0; l<9;l++){
                nd[i][j].f[l] = wght[l]*smAmpl;
                nd[i][j].g[l] = wght[l]*invSposSq;
            }    
            nd[i][j].rho = smAmpl;
            nd[i][j].prssr = 1.0;
            nd[i][j].u[0] = 0.005;
            nd[i][j].u[1] = 0.0;
        }
    }
}

// noch nicht auf neues koordinatensystem geändert
void lattice::pressureDiff(int midX, int midY, int radius, int width, double sigma, double* convAr, int counter){
    int distc;
    double delta;
    double laplace = sigma/double(radius);
    pIn = 0.0;
    pOut = 0.0;
    nodesIn = 0;
    nodesOut = 0;
    for(int i=0; i<lenX; i++){
        for(int j =0; j<lenY; j++){
            distc = sqrt( p2(i-midX) + p2(j-midY));
            if( distc < radius-width ){
                pIn += nd[i][j].prssr;
                nodesIn += 1;
            } else if(distc > radius + width ){
                pOut += nd[i][j].prssr;
                nodesOut +=1;
            }
        }
    }
    pIn = pIn/double(nodesIn);
    pOut = pOut/double(nodesOut);
    delta = pIn - pOut;
    relError = (delta-laplace)/laplace;
    convAr[counter] = relError;
}

// noch nicht auf neues koordinatensystem geändert
bool lattice::checkConvergence(double* convAr, int len, double lmt){
    double sum = 0.0;
    for( int i=0; i<len; i++){
        sum += sqrt(convAr[i]*convAr[i]);
    }
    relError = sqrt(double(sum*sum)/(len*len)); 
    if(  relError < lmt){
        return true;
    } else{
        return false;
    }
}


//calculating new rho, u, p
void lattice::macroscopic(){
    for(int i=startX; i<endX; i++){
        for(int j=startY; j<endY; j++){
            nd[i][j].rho = nd[i][j].fBar[0] + nd[i][j].fBar[1] + nd[i][j].fBar[2] + nd[i][j].fBar[3] 
                + nd[i][j].fBar[4] + nd[i][j].fBar[5] + nd[i][j].fBar[6] + nd[i][j].fBar[7] + nd[i][j].fBar[8];

            nd[i][j].u[0] = (nd[i][j].gBar[1] -nd[i][j].gBar[3] +nd[i][j].gBar[5] -nd[i][j].gBar[6] 
                                -nd[i][j].gBar[7] +nd[i][j].gBar[8] +0.5*kappa*(nd[i][j].drv[1] * nd[i][j].drv[4] - nd[i][j].drv[0] * nd[i][j].drv[3]) )/nd[i][j].rho;
            nd[i][j].u[1] = (nd[i][j].gBar[2] - nd[i][j].gBar[4] +nd[i][j].gBar[5] +nd[i][j].gBar[6] 
                                -nd[i][j].gBar[7] -nd[i][j].gBar[8] +0.5*kappa*(nd[i][j].drv[0] * nd[i][j].drv[4] - nd[i][j].drv[1] * nd[i][j].drv[2]) )/nd[i][j].rho;
            nd[i][j].prssr = spos*spos*(  nd[i][j].gBar[0] +nd[i][j].gBar[1] +nd[i][j].gBar[2] +nd[i][j].gBar[3] +nd[i][j].gBar[4] 
                            +nd[i][j].gBar[5] +nd[i][j].gBar[6] +nd[i][j].gBar[7] +nd[i][j].gBar[8] 
                            +0.5*(nd[i][j].u[0]*nd[i][j].drv[0] + nd[i][j].u[1]*nd[i][j].drv[1])  );
        }
    }
    copyBoundaries();
}

// collides with biased/mixed scheme and streams. filedRho and fieldAux must be called before.
void lattice::preStrCollAndStr(double rhoLq, double rhoVp, double tauLq, double tauVp){

    setDerivatives(1, rhoLq, rhoVp);      //every lattice site knows its derivatives

    double constA = (tauLq - tauVp)/(rhoLq - rhoVp);
	double frc;
	double Gx;
	double Gy;
	double GU;
    double EG;
	double degRate;
	double gradRhoU;
	double gradPsiU;
    double tau;
    double ux;
    double uy;
    double u2;
    double gEqConst;
    double gEq;
    double gam;

	for(int i=startX; i<endX; i++){
        for(int j=startY; j<endY; j++){
            /*
            xp = nd[i][j].nn[0];
            xm = nd[i][j].nn[2];
            xp2 = nd[i][j].nn[4];
            xm2 = nd[i][j].nn[6];
        	yp = nd[i][j].nn[1];
        	ym = nd[i][j].nn[3];
        	yp2 = nd[i][j].nn[5];
        	ym2 = nd[i][j].nn[7];
            */

            xp = i+1;
            xm = i-1;
            xp2 = i+2;
            xm2 = i-2;
            yp = j+1;
            ym = j-1;
            yp2 = j+2;
            ym2 = j-2;

        	// defining repeated quantities:
            ux = nd[i][j].u[0];
            uy = nd[i][j].u[1];
            u2 = ux*ux + uy*uy;
            tau = tauVp + (nd[i][j].rho - rhoVp)*constA;

        	degRate = 1.0/(2.0*tau);
        	gradRhoU = nd[i][j].u[0]*nd[i][j].drv[0] + nd[i][j].u[1]*nd[i][j].drv[1];
        	Gx = 1.5*kappa * (nd[i][j].drv[1] * nd[i][j].drv[4] -nd[i][j].drv[0]*nd[i][j].drv[3]);
        	Gy = 1.5*kappa * (nd[i][j].drv[0]*nd[i][j].drv[4] - nd[i][j].drv[1]*nd[i][j].drv[2]);
        	GU = -nd[i][j].u[0]*Gx -nd[i][j].u[1]*Gy;

       		gradPsiU = nd[i][j].u[0]*derivPsi(i,j,0) + nd[i][j].u[1]*derivPsi(i,j,1);

        	//colliding and streaming g and f:

            //-------------------------e0---------------------------------
            gam = wght[0] * (1.0 - 1.5*u2);          
            gEqConst = 4.0/3.0*nd[i][j].prssr;
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[0] );
            frc = -0.5*gradRhoU*(gam - wght[0]) + GU * gam;
            nd[i][j].gBar[0] = nd[i][j].g[0] + degRate*( gEq - nd[i][j].g[0] ) + frc;           

            frc = -0.5*gradRhoU * gam +1.5*nd[i][j].rho*gradPsiU*gam;
            nd[i][j].fBar[0] = nd[i][j].f[0] + degRate*( nd[i][j].rho*gam - nd[i][j].f[0] ) + frc;          

            //-------------------------e1---------------------------------
            gam = wght[1] + ( ux +1.5*ux*ux -0.5*u2)/3.0; 
            gEqConst = nd[i][j].prssr/3.0;
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[1] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[1]  -  nd[i][j].dirDerivRho[1]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[1] -4.0*nd[ xm ][ j ].dirDerivRho[1] 
                    +nd[ xp ][ yp ].dirDerivRho[1] -nd[ xm ][ ym ].dirDerivRho[1] +nd[ xp ][ ym ].dirDerivRho[1] -nd[ xm ][ yp ].dirDerivRho[1] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[1] -4.0*nd[ i ][ ym ].dirDerivRho[1] 
                    +nd[ xp ][ yp ].dirDerivRho[1] -nd[ xm ][ yp ].dirDerivRho[1] -nd[ xp ][ ym ].dirDerivRho[1] +nd[ xm ][ yp ].dirDerivRho[1])/12.0 );

        	frc = 0.5* (nd[i][j].dirDerivRho[1] - gradRhoU) * (gam - wght[1]) + (EG + GU)*gam;
        	nd[ xp ][ j ].gBar[1] = nd[i][j].g[1] + degRate*( gEq - nd[i][j].g[1] ) + frc;	

            frc = 0.5*(nd[i][j].dirDerivRho[1] - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[1] - gradPsiU)*gam;
            nd[ xp ][ j ].fBar[1] = nd[i][j].f[1] + degRate*( nd[i][j].rho*gam - nd[i][j].f[1] ) + frc;       	

            //-------------------------e2---------------------------------
            gam = wght[2] + ( uy +1.5*uy*uy -0.5*u2)/3.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[2] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[2]  -  nd[i][j].dirDerivRho[2]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[2] -4.0*nd[ xm ][ j ].dirDerivRho[2] 
                    +nd[ xp ][ yp ].dirDerivRho[2] -nd[ xm ][ ym ].dirDerivRho[2] +nd[ xp ][ ym ].dirDerivRho[2] -nd[ xm ][ yp ].dirDerivRho[2] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[2] -4.0*nd[ i ][ ym ].dirDerivRho[2] 
                    +nd[ xp ][ yp ].dirDerivRho[2] -nd[ xm ][ yp ].dirDerivRho[2] -nd[ xp ][ ym ].dirDerivRho[2] +nd[ xm ][ yp ].dirDerivRho[2])/12.0 );

        	frc = 0.5* (nd[i][j].dirDerivRho[2] - gradRhoU) * (gam - wght[2]) + (EG + GU)*gam;
        	nd[ i ][ yp ].gBar[2] = nd[i][j].g[2] + degRate*( gEq - nd[i][j].g[2] ) + frc;	

            frc = 0.5*(nd[i][j].dirDerivRho[2] - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[2] - gradPsiU)*gam;
            nd[ i ][ yp ].fBar[2] = nd[i][j].f[2] + degRate*( nd[i][j].rho*gam - nd[i][j].f[2] ) + frc;       	

            //-------------------------e3---------------------------------
            gam = wght[3] + ( -ux +1.5*ux*ux -0.5*u2)/3.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[3] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[3]  -  nd[i][j].dirDerivRho[3]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[3] -4.0*nd[ xm ][ j ].dirDerivRho[3] 
                    +nd[ xp ][ yp ].dirDerivRho[3] -nd[ xm ][ ym ].dirDerivRho[3] +nd[ xp ][ ym ].dirDerivRho[3] -nd[ xm ][ yp ].dirDerivRho[3] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[3] -4.0*nd[ i ][ ym ].dirDerivRho[3] 
                    +nd[ xp ][ yp ].dirDerivRho[3] -nd[ xm ][ yp ].dirDerivRho[3] -nd[ xp ][ ym ].dirDerivRho[3] +nd[ xm ][ yp ].dirDerivRho[3])/12.0 );

        	frc = 0.5* (nd[i][j].dirDerivRho[3] - gradRhoU) * (gam - wght[3]) + (EG + GU)*gam;
        	nd[ xm ][ j ].gBar[3] = nd[i][j].g[3] + degRate*( gEq - nd[i][j].g[3] ) + frc;		

            frc = 0.5*(nd[i][j].dirDerivRho[3] - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[3] - gradPsiU)*gam;
            nd[ xm ][ j ].fBar[3] = nd[i][j].f[3] + degRate*( nd[i][j].rho*gam - nd[i][j].f[3] ) + frc;

            //-------------------------e4---------------------------------
            gam = wght[4] + ( -uy +1.5*uy*uy -0.5*u2)/3.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[4] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[4]  -  nd[i][j].dirDerivRho[4]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[4] -4.0*nd[ xm ][ j ].dirDerivRho[4] 
                    +nd[ xp ][ yp ].dirDerivRho[4] -nd[ xm ][ ym ].dirDerivRho[4] +nd[ xp ][ ym ].dirDerivRho[4] -nd[ xm ][ yp ].dirDerivRho[4] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[4] -4.0*nd[ i ][ ym ].dirDerivRho[4] 
                    +nd[ xp ][ yp ].dirDerivRho[4] -nd[ xm ][ yp ].dirDerivRho[4] -nd[ xp ][ ym ].dirDerivRho[4] +nd[ xm ][ yp ].dirDerivRho[4])/12.0 );

        	frc = 0.5* (nd[i][j].dirDerivRho[4] - gradRhoU) * (gam - wght[4]) + (EG + GU)*gam;
        	nd[ i ][ ym ].gBar[4] = nd[i][j].g[4] + degRate*( gEq - nd[i][j].g[4] ) + frc;		

            frc = 0.5*(nd[i][j].dirDerivRho[4] - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[4] - gradPsiU)*gam;
            nd[ i ][ ym ].fBar[4] = nd[i][j].f[4] + degRate*( nd[i][j].rho*gam - nd[i][j].f[4] ) + frc;

            //-------------------------e5---------------------------------
            gam = wght[5] + ( ux+uy +u2 +3.0*ux*uy)/12.0; 
            gEqConst = nd[i][j].prssr/12.0;
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[5] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[5]  -  nd[i][j].dirDerivRho[5]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[5] -4.0*nd[ xm ][ j ].dirDerivRho[5] 
                    +nd[ xp ][ yp ].dirDerivRho[5] -nd[ xm ][ ym ].dirDerivRho[5] +nd[ xp ][ ym ].dirDerivRho[5] -nd[ xm ][ yp ].dirDerivRho[5] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[5] -4.0*nd[ i ][ ym ].dirDerivRho[5] 
                    +nd[ xp ][ yp ].dirDerivRho[5] -nd[ xm ][ yp ].dirDerivRho[5] -nd[ xp ][ ym ].dirDerivRho[5] +nd[ xm ][ yp ].dirDerivRho[5])/12.0 );

        	frc = 0.5* (nd[i][j].dirDerivRho[5] - gradRhoU) * (gam - wght[5]) + (EG + GU)*gam;
        	nd[ xp ][ yp ].gBar[5] = nd[i][j].g[5] + degRate*( gEq - nd[i][j].g[5] ) + frc;

            frc = 0.5*(nd[i][j].dirDerivRho[5] - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[5] - gradPsiU)*gam;
            nd[ xp ][ yp ].fBar[5] = nd[i][j].f[5] + degRate*( nd[i][j].rho*gam - nd[i][j].f[5] ) + frc;		

            //-------------------------e6---------------------------------
            gam = wght[6] + ( -ux+uy +u2 -3.0*ux*uy)/12.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[6] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[6]  -  nd[i][j].dirDerivRho[6]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[6] -4.0*nd[ xm ][ j ].dirDerivRho[6] 
                    +nd[ xp ][ yp ].dirDerivRho[6] -nd[ xm ][ ym ].dirDerivRho[6] +nd[ xp ][ ym ].dirDerivRho[6] -nd[ xm ][ yp ].dirDerivRho[6] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[6] -4.0*nd[ i ][ ym ].dirDerivRho[6] 
                    +nd[ xp ][ yp ].dirDerivRho[6] -nd[ xm ][ yp ].dirDerivRho[6] -nd[ xp ][ ym ].dirDerivRho[6] +nd[ xm ][ yp ].dirDerivRho[6])/12.0 );

        	frc = 0.5* (nd[i][j].dirDerivRho[6] - gradRhoU) * (gam - wght[6]) + (EG + GU)*gam;
        	nd[ xm ][ yp ].gBar[6] = nd[i][j].g[6] + degRate*( gEq - nd[i][j].g[6] ) + frc;

            frc = 0.5*(nd[i][j].dirDerivRho[6] - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[6] - gradPsiU)*gam;
            nd[ xm ][ yp ].fBar[6] = nd[i][j].f[6] + degRate*( nd[i][j].rho*gam - nd[i][j].f[6] ) + frc;		

            //-------------------------e7---------------------------------
            gam = wght[7] + ( -ux-uy +u2 +3.0*ux*uy)/12.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[7] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[7]  -  nd[i][j].dirDerivRho[7]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[7] -4.0*nd[ xm ][ j ].dirDerivRho[7] 
                    +nd[ xp ][ yp ].dirDerivRho[7] -nd[ xm ][ ym ].dirDerivRho[7] +nd[ xp ][ ym ].dirDerivRho[7] -nd[ xm ][ yp ].dirDerivRho[7] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[7] -4.0*nd[ i ][ ym ].dirDerivRho[7] 
                    +nd[ xp ][ yp ].dirDerivRho[7] -nd[ xm ][ yp ].dirDerivRho[7] -nd[ xp ][ ym ].dirDerivRho[7] +nd[ xm ][ yp ].dirDerivRho[7])/12.0 );

			frc = 0.5* (nd[i][j].dirDerivRho[7] - gradRhoU) * (gam - wght[7]) + (EG + GU)*gam;
        	nd[ xm ][ ym ].gBar[7] = nd[i][j].g[7] + degRate*( gEq - nd[i][j].g[7] ) + frc;

            frc = 0.5*(nd[i][j].dirDerivRho[7] - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[7] - gradPsiU)*gam;
            nd[ xm ][ ym ].fBar[7] = nd[i][j].f[7] + degRate*( nd[i][j].rho*gam - nd[i][j].f[7] ) + frc;		

            //-------------------------e8---------------------------------
            gam = wght[8] + ( ux-uy +u2 -3.0*ux*uy)/12.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[8] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[8]  -  nd[i][j].dirDerivRho[8]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[8] -4.0*nd[ xm ][ j ].dirDerivRho[8] 
                    +nd[ xp ][ yp ].dirDerivRho[8] -nd[ xm ][ ym ].dirDerivRho[8] +nd[ xp ][ ym ].dirDerivRho[8] -nd[ xm ][ yp ].dirDerivRho[8] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[8] -4.0*nd[ i ][ ym ].dirDerivRho[8] 
                    +nd[ xp ][ yp ].dirDerivRho[8] -nd[ xm ][ yp ].dirDerivRho[8] -nd[ xp ][ ym ].dirDerivRho[8] +nd[ xm ][ yp ].dirDerivRho[8])/12.0 );

        	frc = 0.5* (nd[i][j].dirDerivRho[8] - gradRhoU) * (gam - wght[8]) + (EG + GU)*gam;
        	nd[ xp ][ ym ].gBar[8] = nd[i][j].g[8] + degRate*( gEq - nd[i][j].g[8] ) + frc;	

            frc = 0.5*(nd[i][j].dirDerivRho[8] - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[8] - gradPsiU)*gam;
            nd[ xp ][ ym ].fBar[8] = nd[i][j].f[8] + degRate*( nd[i][j].rho*gam - nd[i][j].f[8] ) + frc;	
        }
    }
    boundaryStream();
}

void lattice::postStrColl(double rhoLq, double rhoVp, double tauLq, double tauVp){

    setDerivatives(0, rhoLq, rhoVp);      //every lattice site knows its derivatives

    double constA = (tauLq - tauVp)/(rhoLq - rhoVp);
    double frc;
	double Gx;
	double Gy;
	double GU;
    double EG;
	double degRate;
    double degRate2;
	double gradRhoU;
	double gradPsiU;
    double tau;
    double ux;
    double uy;
    double u2;
    double gEqConst;
    double gEq;
    double gam;

	for(int i=startX; i<endX; i++){
        for(int j=startY; j<endY; j++){
            xp = i+1;
            xm = i-1;
            xp2 = i+2;
            xm2 = i-2;
            yp = j+1;
            ym = j-1;
            yp2 = j+2;
            ym2 = j-2;
        	
        	// defining repeated quantities:
            ux = nd[i][j].u[0];
            uy = nd[i][j].u[1];
            u2 = ux*ux + uy*uy;
            tau = tauVp + (nd[i][j].rho - rhoVp)*constA;
        	degRate = 1.0/(2.0*tau +1.0);
            degRate2 = tau/(tau+0.5);
        	gradRhoU = nd[i][j].u[0]*nd[i][j].drv[0] + nd[i][j].u[1]*nd[i][j].drv[1];
        	Gx = 1.5*kappa * (nd[i][j].drv[1] * nd[i][j].drv[4] -nd[i][j].drv[0]*nd[i][j].drv[3]);
        	Gy = 1.5*kappa * (nd[i][j].drv[0]*nd[i][j].drv[4] - nd[i][j].drv[1]*nd[i][j].drv[2]);
        	GU = -nd[i][j].u[0]*Gx -nd[i][j].u[1]*Gy;
       		gradPsiU = nd[i][j].u[0]*derivPsi(i,j,0) + nd[i][j].u[1]*derivPsi(i,j,1);




        	//colliding and streaming g and f:
            //----------------e0-----------------------
            gam = wght[0] * (1.0 - 1.5*u2);          
            gEqConst = 4.0/3.0*nd[i][j].prssr;
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[0] );
        	frc = tau/(tau+0.5) *(-0.5*gradRhoU*(gam - wght[0]) + GU * gam);
        	nd[i][j].g[0] = nd[i][j].gBar[0] + degRate*( gEq - nd[i][j].gBar[0] ) + frc;		

            frc = tau/(tau+0.5) *(-0.5*gradRhoU * gam +1.5*nd[i][j].rho*gradPsiU*gam);
            nd[i][j].f[0] = nd[i][j].fBar[0] + degRate*( nd[i][j].rho*gam - nd[i][j].fBar[0] ) + frc;

            //----------------e1-----------------------
            gam = wght[1] + ( ux +1.5*ux*ux -0.5*u2)/3.0; 
            gEqConst = nd[i][j].prssr/3.0;
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[1] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[1]  -  nd[i][j].dirDerivRho[1]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[1] -4.0*nd[ xm ][ j ].dirDerivRho[1] 
                    +nd[ xp ][ yp ].dirDerivRho[1] -nd[ xm ][ ym ].dirDerivRho[1] +nd[ xp ][ ym ].dirDerivRho[1] -nd[ xm ][ yp ].dirDerivRho[1] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[1] -4.0*nd[ i ][ ym ].dirDerivRho[1] 
                    +nd[ xp ][ yp ].dirDerivRho[1] -nd[ xm ][ yp ].dirDerivRho[1] -nd[ xp ][ ym ].dirDerivRho[1] +nd[ xm ][ yp ].dirDerivRho[1])/12.0 );
            
            frc = degRate2*(0.5* (nd[i][j].dirDerivRho[1] - gradRhoU) * (gam - wght[1]) + (EG + GU)*gam);
            nd[ i ][ j ].g[1] = nd[i][j].gBar[1] + degRate*( gEq - nd[i][j].gBar[1] ) + frc;

            frc = degRate2 *(0.5*(nd[i][j].dirDerivRho[1] 
                    - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[1] - gradPsiU)*gam);
            nd[ i ][ j ].f[1] = nd[i][j].fBar[1] + degRate*( nd[i][j].rho*gam - nd[i][j].fBar[1] ) + frc;


            //----------------e2-----------------------
            gam = wght[2] + ( uy +1.5*uy*uy -0.5*u2)/3.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[2] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[2]  -  nd[i][j].dirDerivRho[2]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[2] -4.0*nd[ xm ][ j ].dirDerivRho[2] 
                    +nd[ xp ][ yp ].dirDerivRho[2] -nd[ xm ][ ym ].dirDerivRho[2] +nd[ xp ][ ym ].dirDerivRho[2] -nd[ xm ][ yp ].dirDerivRho[2] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[2] -4.0*nd[ i ][ ym ].dirDerivRho[2] 
                    +nd[ xp ][ yp ].dirDerivRho[2] -nd[ xm ][ yp ].dirDerivRho[2] -nd[ xp ][ ym ].dirDerivRho[2] +nd[ xm ][ yp ].dirDerivRho[2])/12.0 );

            frc = degRate2 *(0.5* (nd[i][j].dirDerivRho[2] - gradRhoU) * (gam - wght[2]) + (EG + GU)*gam);
                nd[ i ][ j ].g[2] = nd[i][j].gBar[2] + degRate*( gEq - nd[i][j].gBar[2] ) + frc;

            frc = degRate2 *(0.5*(nd[i][j].dirDerivRho[2] 
                    - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[2] - gradPsiU)*gam);
            nd[ i ][ j ].f[2] = nd[i][j].fBar[2] + degRate*( nd[i][j].rho*gam - nd[i][j].fBar[2] ) + frc;

            //----------------e3-----------------------
            gam = wght[3] + ( -ux +1.5*ux*ux -0.5*u2)/3.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[3] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[3]  -  nd[i][j].dirDerivRho[3]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[3] -4.0*nd[ xm ][ j ].dirDerivRho[3] 
                    +nd[ xp ][ yp ].dirDerivRho[3] -nd[ xm ][ ym ].dirDerivRho[3] +nd[ xp ][ ym ].dirDerivRho[3] -nd[ xm ][ yp ].dirDerivRho[3] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[3] -4.0*nd[ i ][ ym ].dirDerivRho[3] 
                    +nd[ xp ][ yp ].dirDerivRho[3] -nd[ xm ][ yp ].dirDerivRho[3] -nd[ xp ][ ym ].dirDerivRho[3] +nd[ xm ][ yp ].dirDerivRho[3])/12.0 );

            frc = degRate2 *(0.5* (nd[i][j].dirDerivRho[3] - gradRhoU) * (gam - wght[3]) + (EG + GU)*gam);
                nd[ i ][ j ].g[3] = nd[i][j].gBar[3] + degRate*( gEq - nd[i][j].gBar[3] ) + frc;

            frc = degRate2 *(0.5*(nd[i][j].dirDerivRho[3] 
                    - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[3] - gradPsiU)*gam);
            nd[ i ][ j ].f[3] = nd[i][j].fBar[3] + degRate*( nd[i][j].rho*gam - nd[i][j].fBar[3] ) + frc;
            //----------------e4-----------------------
            gam = wght[4] + ( -uy +1.5*uy*uy -0.5*u2)/3.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[4] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[4]  -  nd[i][j].dirDerivRho[4]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[4] -4.0*nd[ xm ][ j ].dirDerivRho[4] 
                    +nd[ xp ][ yp ].dirDerivRho[4] -nd[ xm ][ ym ].dirDerivRho[4] +nd[ xp ][ ym ].dirDerivRho[4] -nd[ xm ][ yp ].dirDerivRho[4] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[4] -4.0*nd[ i ][ ym ].dirDerivRho[4] 
                    +nd[ xp ][ yp ].dirDerivRho[4] -nd[ xm ][ yp ].dirDerivRho[4] -nd[ xp ][ ym ].dirDerivRho[4] +nd[ xm ][ yp ].dirDerivRho[4])/12.0 );

            frc = degRate2 *(0.5* (nd[i][j].dirDerivRho[4] - gradRhoU) * (gam - wght[4]) + (EG + GU)*gam);
                nd[ i ][ j ].g[4] = nd[i][j].gBar[4] + degRate*( gEq - nd[i][j].gBar[4] ) + frc;

            frc = degRate2 *(0.5*(nd[i][j].dirDerivRho[4] 
                    - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[4] - gradPsiU)*gam);
            nd[ i ][ j ].f[4] = nd[i][j].fBar[4] + degRate*( nd[i][j].rho*gam - nd[i][j].fBar[4] ) + frc;
            //----------------e5-----------------------
            gam = wght[5] + ( ux+uy +u2 +3.0*ux*uy)/12.0; 
            gEqConst = nd[i][j].prssr/12.0;
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[5] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[5]  -  nd[i][j].dirDerivRho[5]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[5] -4.0*nd[ xm ][ j ].dirDerivRho[5] 
                    +nd[ xp ][ yp ].dirDerivRho[5] -nd[ xm ][ ym ].dirDerivRho[5] +nd[ xp ][ ym ].dirDerivRho[5] -nd[ xm ][ yp ].dirDerivRho[5] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[5] -4.0*nd[ i ][ ym ].dirDerivRho[5] 
                    +nd[ xp ][ yp ].dirDerivRho[5] -nd[ xm ][ yp ].dirDerivRho[5] -nd[ xp ][ ym ].dirDerivRho[5] +nd[ xm ][ yp ].dirDerivRho[5])/12.0 );

            frc = degRate2 *(0.5* (nd[i][j].dirDerivRho[5] - gradRhoU) * (gam - wght[5]) + (EG + GU)*gam);
                nd[ i ][ j ].g[5] = nd[i][j].gBar[5] + degRate*( gEq - nd[i][j].gBar[5] ) + frc;

            frc = degRate2 *(0.5*(nd[i][j].dirDerivRho[5] 
                    - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[5] - gradPsiU)*gam);
            nd[ i ][ j ].f[5] = nd[i][j].fBar[5] + degRate*( nd[i][j].rho*gam - nd[i][j].fBar[5] ) + frc;
            //----------------e6-----------------------
            gam = wght[6] + ( -ux+uy +u2 -3.0*ux*uy)/12.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[6] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[6]  -  nd[i][j].dirDerivRho[6]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[6] -4.0*nd[ xm ][ j ].dirDerivRho[6] 
                    +nd[ xp ][ yp ].dirDerivRho[6] -nd[ xm ][ ym ].dirDerivRho[6] +nd[ xp ][ ym ].dirDerivRho[6] -nd[ xm ][ yp ].dirDerivRho[6] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[6] -4.0*nd[ i ][ ym ].dirDerivRho[6] 
                    +nd[ xp ][ yp ].dirDerivRho[6] -nd[ xm ][ yp ].dirDerivRho[6] -nd[ xp ][ ym ].dirDerivRho[6] +nd[ xm ][ yp ].dirDerivRho[6])/12.0 );

            frc = degRate2 *(0.5* (nd[i][j].dirDerivRho[6] - gradRhoU) * (gam - wght[6]) + (EG + GU)*gam);
                nd[ i ][ j ].g[6] = nd[i][j].gBar[6] + degRate*( gEq - nd[i][j].gBar[6] ) + frc;

            frc = degRate2 *(0.5*(nd[i][j].dirDerivRho[6] 
                    - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[6] - gradPsiU)*gam);
            nd[ i ][ j ].f[6] = nd[i][j].fBar[6] + degRate*( nd[i][j].rho*gam - nd[i][j].fBar[6] ) + frc;
            //----------------e7-----------------------
            gam = wght[7] + ( -ux-uy +u2 +3.0*ux*uy)/12.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[7] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[7]  -  nd[i][j].dirDerivRho[7]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[7] -4.0*nd[ xm ][ j ].dirDerivRho[7] 
                    +nd[ xp ][ yp ].dirDerivRho[7] -nd[ xm ][ ym ].dirDerivRho[7] +nd[ xp ][ ym ].dirDerivRho[7] -nd[ xm ][ yp ].dirDerivRho[7] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[7] -4.0*nd[ i ][ ym ].dirDerivRho[7] 
                    +nd[ xp ][ yp ].dirDerivRho[7] -nd[ xm ][ yp ].dirDerivRho[7] -nd[ xp ][ ym ].dirDerivRho[7] +nd[ xm ][ yp ].dirDerivRho[7])/12.0 );

            frc = degRate2 *(0.5* (nd[i][j].dirDerivRho[7] - gradRhoU) * (gam - wght[7]) + (EG + GU)*gam);
                nd[ i ][ j ].g[7] = nd[i][j].gBar[7] + degRate*( gEq - nd[i][j].gBar[7] ) + frc;

            frc = degRate2 *(0.5*(nd[i][j].dirDerivRho[7] 
                    - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[7] - gradPsiU)*gam);
            nd[ i ][ j ].f[7] = nd[i][j].fBar[7] + degRate*( nd[i][j].rho*gam - nd[i][j].fBar[7] ) + frc;
            //----------------e8-----------------------
            gam = wght[8] + ( ux-uy +u2 -3.0*ux*uy)/12.0; 
            gEq =  gEqConst + nd[i][j].rho*( gam -wght[8] );
            EG = 1.5*kappa*( nd[i][j].dirDerivB[8]  -  nd[i][j].dirDerivRho[8]*nd[i][j].drv[5] 
                - nd[i][j].drv[0]*( 4.0*nd[ xp ][ j ].dirDerivRho[8] -4.0*nd[ xm ][ j ].dirDerivRho[8] 
                    +nd[ xp ][ yp ].dirDerivRho[8] -nd[ xm ][ ym ].dirDerivRho[8] +nd[ xp ][ ym ].dirDerivRho[8] -nd[ xm ][ yp ].dirDerivRho[8] )/12.0 
                - nd[i][j].drv[1]*(4.0*nd[ i ][ yp ].dirDerivRho[8] -4.0*nd[ i ][ ym ].dirDerivRho[8] 
                    +nd[ xp ][ yp ].dirDerivRho[8] -nd[ xm ][ yp ].dirDerivRho[8] -nd[ xp ][ ym ].dirDerivRho[8] +nd[ xm ][ yp ].dirDerivRho[8])/12.0 );

            frc = degRate2 *(0.5* (nd[i][j].dirDerivRho[8] - gradRhoU) * (gam - wght[8]) + (EG + GU)*gam);
                nd[ i ][ j ].g[8] = nd[i][j].gBar[8] + degRate*( gEq - nd[i][j].gBar[8] ) + frc;

            frc = degRate2 *(0.5*(nd[i][j].dirDerivRho[8] 
                    - gradRhoU)*gam -1.5*nd[i][j].rho*( nd[i][j].dirDerivPsi[8] - gradPsiU)*gam);
            nd[ i ][ j ].f[8] = nd[i][j].fBar[8] + degRate*( nd[i][j].rho*gam - nd[i][j].fBar[8] ) + frc;
        }
    }
}

// central difference operator: dir = false: derivation wrt x, dir = true: derivation wrt y
double lattice::derivRho(int x, int y, bool dir){
	xp = x+1;
    xm = x-1;
    yp = y+1;
    ym = y-1;
	double a;
	if(dir){
		a = ( 4.0*nd[ x ][ yp ].rho - 4.0*nd[ x ][ ym ].rho + nd[ xp ][ yp ].rho - nd[ xm ][ ym ].rho + nd[ xm ][ yp ].rho 
				- nd[ xp ][ ym ].rho)/12.0;
	} else {
		a = ( 4.0*nd[ xp ][ y ].rho - 4.0*nd[ xm ][ y ].rho + nd[ xp ][ yp ].rho - nd[ xm ][ ym ].rho - nd[ xm ][ yp ].rho 
				+ nd[ xp ][ ym ].rho)/12.0;
	}
	return a;
}

// second derivative
double lattice::deriv2Rho(int x, int y, bool dir){
    xp = x+1;
    xm = x-1;
    xp2 = x+2;
    xm2 = x-2;
    yp = y+1;
    ym = y-1;
    yp2 = y+2;
    ym2 = y-2;

	double a;
	if(dir){
		a = ( nd[ xm2 ][ yp2 ].rho +nd[ xp2 ][ yp2 ].rho  +nd[ xm2 ][ ym2 ].rho+nd[ xp2 ][ ym2 ].rho)/144.0 
			+ (9.0*nd[ x ][ yp2 ].rho -nd[ xm2 ][ y ].rho -nd[ xp2 ][ y ].rho +9.0*nd[ x ][ ym2 ].rho)/72.0
			+ (2.0*nd[ xm ][ yp2 ].rho +2.0*nd[ xp ][ yp2 ].rho -4.0*nd[ xm ][ y ].rho	
				-4.0*nd[ xp ][ y ].rho +2.0*nd[ xm ][ ym2 ].rho +2.0*nd[ xp ][ ym2 ].rho -9.0*nd[ x ][ y ].rho)/36.0;
	} else {
		a = ( nd[ xm2 ][ yp2 ].rho +nd[ xp2 ][ yp2 ].rho +nd[ xm2 ][ ym2 ].rho + nd[ xp2 ][ ym2 ].rho)/144.0 
			+ (	-nd[ x ][ yp2 ].rho +9.0*nd[ xm2 ][ y ].rho +9.0*nd[ xp2 ][y ].rho -nd[ x ][ ym2 ].rho)/72.0
			+ (	2.0*nd[ xm2 ][ yp ].rho -4.0*nd[ x ][ yp ].rho +2.0*nd[ xp2 ][ yp ].rho
				 + 2.0*nd[ xm2 ][ ym ].rho -4.0*nd[ x ][ ym ].rho +2.0*nd[ xp2 ][ ym ].rho -9.0*nd[ x ][ y ].rho)/36.0;
	}
	return a;
}
// second derivative: mixed 
double lattice::deriv2RhoM( int x, int y){
	xp = x+1;
    xm = x-1;
    yp = y+1;
    ym = y-1;
    xp2 = x+2;
    xm2 = x-2;
    yp2 = y+2;
    ym2 = y-2;
	double a = ( nd[ xp ][ yp ].rho -nd[ xm ][ yp ].rho +nd[ xm ][ ym ].rho -nd[ xp ][ ym ].rho	)/9.0 
				+( nd[ xp2 ][ yp ].rho +nd[ xp ][ yp2 ].rho -nd[ xm ][ yp2 ].rho -nd[ xm2 ][ yp ].rho +nd[ xm2 ][ ym ].rho +nd[ xm ][ ym2 ].rho	
					-nd[ xp ][ ym2 ].rho -nd[ xp2 ][ ym ].rho)/36.0 
				+ (	nd[ xp2 ][ yp2 ].rho -nd[ xm2 ][ yp2 ].rho +nd[ xm2 ][ ym2 ].rho -nd[ xp2 ][ ym2 ].rho)/144.0;
	return a;
}

double lattice::laplaceRho(int x, int y){
	xp = x+1;
    xm = x-1;
    yp = y+1;
    ym = y-1;
	double a = ( nd[ xp ][ yp ].rho +nd[ xm ][ yp ].rho +nd[ xp ][ ym ].rho +nd[ xm ][ ym ].rho +4.0*nd[ xp ][ y ].rho +4.0*nd[ xm ][ y ].rho 
			+4.0*nd[ x ][ yp ].rho +4.0*nd[ x ][ ym ].rho -20.0*nd[ x ][ y ].rho)/6.0;
	return a;
}

// derivatives for the auxiliary term in f distribution:
double lattice::derivPsi(int x, int y, bool dir){
	xp = x+1;
    xm = x-1;
    yp = y+1;
    ym = y-1;
	double a;
	if(dir){
		a = ( 	4.0*nd[ x ][ yp ].psi - 4.0*nd[ x ][ ym ].psi  +  nd[ xp ][ yp ].psi - nd[ xm ][ ym ].psi 
				+ nd[ xm ][ yp ].psi - nd[ xp ][ ym ].psi)/12.0;
	} else {
		a = (	4.0*nd[ xp ][ y ].psi - 4.0*nd[ xm ][ y ].psi +  nd[ xp ][ yp ].psi - nd[ xm ][ ym ].psi	
				- nd[ xm ][ yp ].psi + nd[ xp ][ ym ].psi)/12.0;
	}
	return a;
}

// directional derivatives modus = 1: mixed/biased;  modus = 0: 2nd central
double lattice::dirDerivRho(int x, int y, int dir, bool modus){
    double a;
    xp = x+1;
    xm = x-1;
    yp = y+1;
    ym = y-1;
    xp2 = x+2;
    xm2 = x-2;
    yp2 = y+2;
    ym2 = y-2;
    if(modus){
        if(dir < 1 || 8 < dir){
            printf("ERROR IN dirDerivRho: invalid direction %d\n",dir);
            return 0.0;
        }
        if(dir == 1){
            a = 0.5* ( -nd[ xp2 ][ y ].rho +4.0*nd[ xp ][ y ].rho -3.0*nd[ x ][ y ].rho );
            if( (nd[ xp2 ][ y ].rho - nd[ x ][ y ].rho) *a < 0.0 ){
                return 0.5*( nd[ xp ][ y ].rho - nd[ xm ][ y ].rho);
            } else {
                return a;
            }
        }
        if(dir == 2){
            a = 0.5* ( -nd[ x ][ yp2 ].rho +4.0*nd[ x ][ yp ].rho -3.0*nd[ x ][ y ].rho );
            if( (nd[ x ][ yp2 ].rho - nd[ x ][ y ].rho) *a < 0.0 ){
                return 0.5*( nd[ x ][ yp ].rho - nd[ x ][ ym ].rho);
            } else {
                return a;
            }
        }
        if(dir == 3){
            a = 0.5* ( -nd[ xm2 ][ y ].rho +4.0*nd[ xm ][ y ].rho -3.0*nd[ x ][ y ].rho );
            if( (nd[ xm2 ][ y ].rho - nd[ x ][ y ].rho) *a < 0.0 ){
                return -0.5*( nd[ xp ][ y ].rho - nd[ xm ][ y ].rho);
            } else {
                return a;
            }
        }
        if(dir == 4){
            a = 0.5* ( -nd[ x ][ ym2 ].rho +4.0*nd[ x ][ ym ].rho -3.0*nd[ x ][ y ].rho );
            if( (nd[ x ][ ym2 ].rho - nd[ x ][ y ].rho) *a < 0.0 ){
                return 0.5*( nd[ x ][ ym ].rho - nd[ x ][ yp ].rho);
            } else {
                return a;
            }
        }
        if(dir == 5){
            a = 0.5* ( -nd[ xp2 ][ yp2 ].rho +4.0*nd[ xp ][ yp ].rho -3.0*nd[ x ][ y ].rho );
            if( (nd[ xp2 ][ yp2 ].rho - nd[ x ][ y ].rho) *a < 0.0 ){
                return 0.5*( nd[ xp ][ yp ].rho - nd[ xm ][ ym ].rho);
            } else {
                return a;
            }
        }
        if(dir == 6){
            a = 0.5* ( -nd[ xm2 ][ yp2 ].rho +4.0*nd[ xm ][ yp ].rho -3.0*nd[ x ][ y ].rho );
            if( (nd[ xm2 ][ yp2 ].rho - nd[ x ][ y ].rho) *a < 0.0 ){
                return 0.5*( nd[ xm ][ yp ].rho - nd[ xp ][ ym ].rho);
            } else {
                return a;
            }
        }
        if(dir == 7){
            a = 0.5* ( -nd[ xm2 ][ ym2 ].rho +4.0*nd[ xm ][ ym ].rho -3.0*nd[ x ][ y ].rho );
            if( (nd[ xm2 ][ ym2 ].rho - nd[ x ][ y ].rho) *a < 0.0 ){
                return 0.5*( nd[ xm ][ ym ].rho - nd[ xp ][ yp ].rho);
            } else {
                return a;
            }
        }
        if(dir == 8){
            a = 0.5* ( -nd[ xp2 ][ ym2 ].rho +4.0*nd[ xp ][ ym ].rho -3.0*nd[ x ][ y ].rho );
            if( (nd[ xp2 ][ ym2 ].rho - nd[ x ][ y ].rho) *a < 0.0 ){
                return 0.5*( nd[ xp ][ ym ].rho - nd[ xm ][ yp ].rho);
            } else {
                return a;
            }
        }
    }else{
        if(dir < 1 || 8 < dir){
            printf("ERROR IN dirDerivRho: invalid direction %d\n",dir);
            return 0.0;
        }
        if(dir == 1){
            return 0.5*( nd[ xp ][ y ].rho - nd[ xm ][ y ].rho);
        }
        if(dir == 2){
            return 0.5*( nd[ x ][ yp ].rho - nd[ x ][ ym ].rho);
        }
        if(dir == 3){
            return -0.5*( nd[ xp ][ y ].rho - nd[ xm ][ y ].rho);
        }
        if(dir == 4){
            return 0.5*( nd[ x ][ ym ].rho - nd[ x ][ yp ].rho);
        }
        if(dir == 5){
            return 0.5*( nd[ xp ][ yp ].rho - nd[ xm ][ ym ].rho);
        }
        if(dir == 6){
            return 0.5*( nd[ xm ][ yp ].rho - nd[ xp ][ ym ].rho);
        }
        if(dir == 7){
            return 0.5*( nd[ xm ][ ym ].rho - nd[ xp ][ yp ].rho);
        }
        if(dir == 8){
            return 0.5*( nd[ xp ][ ym ].rho - nd[ xm ][ yp ].rho);
        }
    }
    return 0.0;
}

double lattice::dirDerivB(int x, int y, int dir, bool modus){
    double a;
    xp = x+1;
    xm = x-1;
    yp = y+1;
    ym = y-1;
    xp2 = x+2;
    xm2 = x-2;
    yp2 = y+2;
    ym2 = y-2;
    if(modus){ 
        if(dir < 1 || 8 < dir){
            printf("ERROR IN dirDerivRho: invalid direction %d\n",dir);
            return 0.0;
        }
        if(dir == 1){
            a = 0.5* ( -nd[ xp2 ][ y ].b +4.0*nd[ xp ][ y ].b -3.0*nd[ x ][ y ].b );
            if( (nd[ xp2 ][ y ].b - nd[ x ][ y ].b) *a < 0.0 ){
                return 0.5*( nd[ xp ][ y ].b - nd[ xm ][ y ].b);
            } else {
                return a;
            }
        }
        if(dir == 2){
            a = 0.5* ( -nd[ x ][ yp2 ].b +4.0*nd[ x ][ yp ].b -3.0*nd[ x ][ y ].b );
            if( (nd[ x ][ yp2 ].b - nd[ x ][ y ].b) *a < 0.0 ){
                return 0.5*( nd[ x ][ yp ].b - nd[ x ][ ym ].b);
            } else {
                return a;
            }
        }
        if(dir == 3){
            a = 0.5* ( -nd[ xm2 ][ y ].b +4.0*nd[ xm ][ y ].b -3.0*nd[ x ][ y ].b );
            if( (nd[ xm2 ][ y ].b - nd[ x ][ y ].b) *a < 0.0 ){
                return -0.5*( nd[ xp ][ y ].b - nd[ xm ][ y ].b);
            } else {
                return a;
            }
        }
        if(dir == 4){
            a = 0.5* ( -nd[ x ][ ym2 ].b +4.0*nd[ x ][ ym ].b -3.0*nd[ x ][ y ].b );
            if( (nd[ x ][ ym2 ].b - nd[ x ][ y ].b) *a < 0.0 ){
                return 0.5*( nd[ x ][ ym ].b - nd[ x ][ yp ].b);
            } else {
                return a;
            }
        }
        if(dir == 5){
            a = 0.5* ( -nd[ xp2 ][ yp2 ].b +4.0*nd[ xp ][ yp ].b -3.0*nd[ x ][ y ].b );
            if( (nd[ xp2 ][ yp2 ].b - nd[ x ][ y ].b) *a < 0.0 ){
                return 0.5*( nd[ xp ][ yp ].b - nd[ xm ][ ym ].b);
            } else {
                return a;
            }
        }
        if(dir == 6){
            a = 0.5* ( -nd[ xm2 ][ yp2 ].b +4.0*nd[ xm ][ yp ].b -3.0*nd[ x ][ y ].b );
            if( (nd[ xm2 ][ yp2 ].b - nd[ x ][ y ].b) *a < 0.0 ){
                return 0.5*( nd[ xm ][ yp ].b - nd[ xp ][ ym ].b);
            } else {
                return a;
            }
        }
        if(dir == 7){
            a = 0.5* ( -nd[ xm2 ][ ym2 ].b +4.0*nd[ xm ][ ym ].b -3.0*nd[ x ][ y ].b );
            if( (nd[ xm2 ][ ym2 ].b - nd[ x ][ y ].b) *a < 0.0 ){
                return 0.5*( nd[ xm ][ ym ].b - nd[ xp ][ yp ].b);
            } else {
                return a;
            }
        }
        if(dir == 8){
            a = 0.5* ( -nd[ xp2 ][ ym2 ].b +4.0*nd[ xp ][ ym ].b -3.0*nd[ x ][ y ].b );
            if( (nd[ xp2 ][ ym2 ].b - nd[ x ][ y ].b) *a < 0.0 ){
                return 0.5*( nd[ xp ][ ym ].b - nd[ xm ][ yp ].b);
            } else {
                return a;
            }
        }
    }else{
        if(dir < 1 || 8 < dir){
            printf("ERROR IN dirDerivRho: invalid direction %d\n",dir);
            return 0.0;
        }
        if(dir == 1){
            return 0.5*( nd[ xp ][ y ].b - nd[ xm ][ y ].b);
        }
        if(dir == 2){
            return 0.5*( nd[ x ][ yp ].b - nd[ x ][ ym ].b);
        }
        if(dir == 3){
            return -0.5*( nd[ xp ][ y ].b - nd[ xm ][ y ].b);
        }
        if(dir == 4){
            return 0.5*( nd[ x ][ ym ].b - nd[ x ][ yp ].b);
        }
        if(dir == 5){
            return 0.5*( nd[ xp ][ yp ].b - nd[ xm ][ ym ].b);
        }
        if(dir == 6){
            return 0.5*( nd[ xm ][ yp ].b - nd[ xp ][ ym ].b);
        }
        if(dir == 7){
            return 0.5*( nd[ xm ][ ym ].b - nd[ xp ][ yp ].b);
        }
        if(dir == 8){
            return 0.5*( nd[ xp ][ ym ].b - nd[ xm ][ yp ].b);
        }
    }
    return 0.0;
}

double lattice::dirDerivPsi(int x, int y, int dir, bool modus){
    double a;
    xp = x+1;
    xm = x-1;
    yp = y+1;
    ym = y-1;
    xp2 = x+2;
    xm2 = x-2;
    yp2 = y+2;
    ym2 = y-2;
    if(modus){
        if(dir < 1 || 8 < dir){
            printf("ERROR IN dirDerivRho: invalid direction %d\n",dir);
            return 0.0;
        }
        if(dir == 1){
            a = 0.5* ( -nd[ xp2 ][ y ].psi +4.0*nd[ xp ][ y ].psi -3.0*nd[ x ][ y ].psi );
            if( (nd[ xp2 ][ y ].psi - nd[ x ][ y ].psi) *a < 0.0 ){
                return 0.5*( nd[ xp ][ y ].psi - nd[ xm ][ y ].psi);
            } else {
                return a;
            }
        }
        if(dir == 2){
            a = 0.5* ( -nd[ x ][ yp2 ].psi +4.0*nd[ x ][ yp ].psi -3.0*nd[ x ][ y ].psi );
            if( (nd[ x ][ yp2 ].psi - nd[ x ][ y ].psi) *a < 0.0 ){
                return 0.5*( nd[ x ][ yp ].psi - nd[ x ][ ym ].psi);
            } else {
                return a;
            }
        }
        if(dir == 3){
            a = 0.5* ( -nd[ xm2 ][ y ].psi +4.0*nd[ xm ][ y ].psi -3.0*nd[ x ][ y ].psi );
            if( (nd[ xm2 ][ y ].psi - nd[ x ][ y ].psi) *a < 0.0 ){
                return -0.5*( nd[ xp ][ y ].psi - nd[ xm ][ y ].psi);
            } else {
                return a;
            }
        }
        if(dir == 4){
            a = 0.5* ( -nd[ x ][ ym2 ].psi +4.0*nd[ x ][ ym ].psi -3.0*nd[ x ][ y ].psi );
            if( (nd[ x ][ ym2 ].psi - nd[ x ][ y ].psi) *a < 0.0 ){
                return 0.5*( nd[ x ][ ym ].psi - nd[ x ][ yp ].psi);
            } else {
                return a;
            }
        }
        if(dir == 5){
            a = 0.5* ( -nd[ xp2 ][ yp2 ].psi +4.0*nd[ xp ][ yp ].psi -3.0*nd[ x ][ y ].psi );
            if( (nd[ xp2 ][ yp2 ].psi - nd[ x ][ y ].psi) *a < 0.0 ){
                return 0.5*( nd[ xp ][ yp ].psi - nd[ xm ][ ym ].psi);
            } else {
                return a;
            }
        }
        if(dir == 6){
            a = 0.5* ( -nd[ xm2 ][ yp2 ].psi +4.0*nd[ xm ][ yp ].psi -3.0*nd[ x ][ y ].psi );
            if( (nd[ xm2 ][ yp2 ].psi - nd[ x ][ y ].psi) *a < 0.0 ){
                return 0.5*( nd[ xm ][ yp ].psi - nd[ xp ][ ym ].psi);
            } else {
                return a;
            }
        }
        if(dir == 7){
            a = 0.5* ( -nd[ xm2 ][ ym2 ].psi +4.0*nd[ xm ][ ym ].psi -3.0*nd[ x ][ y ].psi );
            if( (nd[ xm2 ][ ym2 ].psi - nd[ x ][ y ].psi) *a < 0.0 ){
                return 0.5*( nd[ xm ][ ym ].psi - nd[ xp ][ yp ].psi);
            } else {
                return a;
            }
        }
        if(dir == 8){
            a = 0.5* ( -nd[ xp2 ][ ym2 ].psi +4.0*nd[ xp ][ ym ].psi -3.0*nd[ x ][ y ].psi );
            if( (nd[ xp2 ][ ym2 ].psi - nd[ x ][ y ].psi) *a < 0.0 ){
                return 0.5*( nd[ xp ][ ym ].psi - nd[ xm ][ yp ].psi);
            } else {
                return a;
            }
        }
    }else{
        if(dir < 1 || 8 < dir){
            printf("ERROR IN dirDerivRho: invalid direction %d\n",dir);
            return 0.0;
        }
        if(dir == 1){
            return 0.5*( nd[ xp ][ y ].psi - nd[ xm ][ y ].psi);
        }
        if(dir == 2){
            return 0.5*( nd[ x ][ yp ].psi - nd[ x ][ ym ].psi);
        }
        if(dir == 3){
            return -0.5*( nd[ xp ][ y ].psi - nd[ xm ][ y ].psi);
        }
        if(dir == 4){
            return 0.5*( nd[ x ][ ym ].psi - nd[ x ][ yp ].psi);
        }
        if(dir == 5){
            return 0.5*( nd[ xp ][ yp ].psi - nd[ xm ][ ym ].psi);
        }
        if(dir == 6){
            return 0.5*( nd[ xm ][ yp ].psi - nd[ xp ][ ym ].psi);
        }
        if(dir == 7){
            return 0.5*( nd[ xm ][ ym ].psi - nd[ xp ][ yp ].psi);
        }
        if(dir == 8){
            return 0.5*( nd[ xp ][ ym ].psi - nd[ xm ][ yp ].psi);
        }
    }
    return 0.0;
}

// calculates new auxiliary quantities b, psi, only to be called from setDerivatives
void lattice::fieldAux(double rhoLq, double rhoVp){
    double chemPot;
    for(int i=0; i<lenX; i++){
        for(int j=0; j<lenY; j++){
            chemPot = 4.0*beta*(nd[i][j].rho - rhoVp)*(nd[i][j].rho - rhoLq)*(nd[i][j].rho -0.5*(rhoVp + rhoLq));
            nd[i][j].psi = chemPot - kappa*nd[i][j].drv[5];
            nd[i][j].b =  nd[i][j].drv[0]*nd[i][j].drv[0] + nd[i][j].drv[1]*nd[i][j].drv[1];
        }
    }
}

// modus = 1: mixed/biased;  modus = 0: 2nd central
void lattice::setDerivatives(bool modus, double rhoLq, double rhoVp){
    fieldAux( rhoLq, rhoVp);                   // updating auxiliary quantities b and psi for directional derivatives in collision terms
    for (int i = startX; i < endX; i++){
        for(int j= startY; j<endY; j++){
            for(int l = 1; l<9; l++){       // directional derivatives on Rho and b
                nd[i][j].dirDerivRho[l] = dirDerivRho(i,j,l,modus);
                nd[i][j].dirDerivB[l] = dirDerivB(i,j,l,modus);
                nd[i][j].dirDerivPsi[l] = dirDerivPsi(i,j,l,modus);
            }
            // defining derivatives:
            nd[i][j].drv[0] = derivRho(i,j,0);           // del_x
            nd[i][j].drv[1] = derivRho(i,j,1);
            nd[i][j].drv[2] = deriv2Rho(i,j,0);      // del_xx
            nd[i][j].drv[3] = deriv2Rho(i,j,1);      
            nd[i][j].drv[4] = deriv2RhoM(i,j);           // del_xy
            nd[i][j].drv[5] = laplaceRho(i,j);
        }
    }
}
/*
//to be called after setDerivatives. adjusts derivatives at fixed boundaries.
void lattice::setBoundaryDeriv(){
    // hard wall at right side:
    for (int j=1; )
    nd[i][j].dirDerivRho[l] = dirDerivRho(i,j,l,modus);
    nd[i][j].dirDerivB[l] = dirDerivB(i,j,l,modus);
    nd[i][j].dirDerivPsi[l] = dirDerivPsi(i,j,l,modus);

}
*/

/*
void lattice::stream(){
    for(int i=0; i<lenX; i++){
        xp = pb(i+1,lenX);
       	xm = pb(i-1,lenX);
        for(int j =0; j<lenY; j++){
        	yp = pb(j+1,lenY);
        	ym = pb(j-1,lenY);

            nd[i][j].g[1] = nd[ xm ][ j ].gBar[1];
            nd[i][j].g[2] = nd[ i ][ ym ].gBar[2];
            nd[i][j].g[3] = nd[ xp ][ j ].gBar[3];
            nd[i][j].g[4] = nd[ i ][ yp ].gBar[4];
            nd[i][j].g[5] = nd[ xm ][ ym ].gBar[5];
            nd[i][j].g[6] = nd[ xp ][ ym ].gBar[6];
            nd[i][j].g[7] = nd[ xp ][ yp ].gBar[7];
            nd[i][j].g[8] = nd[ xm ][ yp ].gBar[8];

            nd[i][j].f[1] = nd[ xm ][ j ].fBar[1];
            nd[i][j].f[2] = nd[ i ][ ym ].fBar[2];
            nd[i][j].f[3] = nd[ xp ][ j ].fBar[3];
            nd[i][j].f[4] = nd[ i ][ yp ].fBar[4];
            nd[i][j].f[5] = nd[ xm ][ ym ].fBar[5];
            nd[i][j].f[6] = nd[ xp ][ ym ].fBar[6];
            nd[i][j].f[7] = nd[ xp ][ yp ].fBar[7];
            nd[i][j].f[8] = nd[ xm ][ yp ].fBar[8];
        }
    }
}
*/

/*
// saves the partial derivatives at each lattice site, so that they don't need to be calculated several times
void lattice::fieldDeriv(){
	for (int i = 0; i < lenX; ++i) {
		for(int j=0; j<lenY; j++){
			nd[i][j].drv[0] = derivC(i,j,0);
			nd[i][j].drv[1] = derivC(i,j,1);
			nd[i][j].drv[2] = derivB(i,j,0);
			nd[i][j].drv[3] = derivB(i,j,1);

			nd[i][j].drv[6] = deriv2C(i,j,0);
			nd[i][j].drv[7] = deriv2C(i,j,1);
			nd[i][j].drv[8] = deriv2M(i,j);
			nd[i][j].drv[9] = laplaceOP(i,j);
			nd[i][j].drv[10] = derivXXC(i,j,0);
			nd[i][j].drv[11] = derivXXC(i,j,1);
			nd[i][j].drv[12] = derivXXB(i,j,0);
			nd[i][j].drv[13] = derivXXB(i,j,1);
		}
	}
	for (int i = 0; i < lenX; ++i) {
		for(int j=0; j<lenY; j++){
			if(nd[i][j].drv[2] * nd[ pb( i+1,lenX ) ][ j ].drv[0] >= 0.0){
				nd[i][j].drv[4] = nd[i][j].drv[2]; 
			} else {
				nd[i][j].drv[4] = nd[i][j].drv[0];
			}

			if(nd[i][j].drv[3] * nd[ i ][ pb(j+1,lenY) ].drv[1] >= 0.0){
				nd[i][j].drv[5] = nd[i][j].drv[3]; 
			} else {
				nd[i][j].drv[5] = nd[i][j].drv[1];
			}

			if(nd[i][j].drv[12] * nd[ pb(i+1,lenX) ][j].drv[10] >= 0.0 ){
				nd[i][j].drv[14] = nd[i][j].drv[12];
			} else {
				nd[i][j].drv[14] = nd[i][j].drv[10];
			}

			if(nd[i][j].drv[13] * nd[i][ pb(j+1,lenY) ].drv[11] >= 0.0){
				nd[i][j].drv[15] = nd[i][j].drv[13];
			} else {
				nd[i][j].drv[15] = nd[i][j].drv[11];
			}
		}
	}
}
*/

/*
// biased difference:
double lattice::derivXXB(int x, int y, bool dir){
	double a;
	if(dir){
		a = (	-nd[ pb(x-1,lenX) ][ pb(y+2,lenY) ].aux -4.0*nd[ x ][ pb(y+2,lenY) ].aux -nd[ pb(x+1,lenX) ][ pb(y+2,lenY) ].aux
				-8.0*nd[ pb(x-1,lenX) ][ pb(y+1,lenY) ].aux +40.0*nd[ x ][ pb(y+1,lenY) ].aux -8.0*nd[ pb(x+1,lenX) ][ pb(y+1,lenY) ].aux 
				-3.0*nd[ pb(x-1,lenX) ][ y ].aux -12.0*nd[ x ][ y ].aux	-3.0*nd[ pb(x+1,lenX) ][ y ].aux)/12.0;
	} else {
		a = (	-3.0*nd[ x ][ pb(y+1,lenY) ].aux -8.0*nd[ pb(x+1,lenX) ][ pb(y+1,lenY) ].aux -nd[ pb(x+2,lenX) ][ pb(y+1,lenY) ].aux 
				-12.0*nd[ x ][ y ].aux + 40.0*nd[ pb(x+1,lenX) ][ y ].aux -4.0*nd[ pb(x+2,lenX) ][ y ].aux 
				-3.0*nd[ x ][ pb(y-1,lenY) ].aux -8.0*nd[ pb(x+1,lenX) ][ pb(y-1,lenY) ].aux -nd[ pb(x+2,lenX) ][ pb(y-1,lenY) ].aux	)/12.0;
	}
	return a;
}
*/

/*  //one dimension central and biased deifferences
double lattice::deriv2C(int x, int y, bool dir){
    if(dir){
        return 0.5*( nd[ x ][ pb(y+1,lenY) ].rho - nd[ x ][ pb(y-1,lenY) ].rho );
    }
    return 0.5*( nd[ pb(x+1,lenX) ][ y ].rho - nd[ pb(x-1,lenX) ][ y ].rho );
}

double lattice::deriv2B(int x, int y, bool dir){
    if(dir){
        return 0.5*( -nd[ x ][ pb(y+2,lenY) ].rho + 4.0 * nd[ x ][ pb(y+1,lenY) ].rho - 3.0 * nd[ x ][ y ].rho );
    }
   return 0.5*( -nd[ pb(x+2,lenX) ][ y ].rho + 4.0 * nd[ pb(x+1,lenX) ][ y ].rho - 3.0 * nd[ x ][ y ].rho );
}
*/

/*
// biased difference:
double lattice::derivB(int x, int y, bool dir){
	double a;
	if(dir){
		a = (	-nd[ pb(x-1,lenX) ][ pb(y+2,lenY) ].rho -4.0*nd[ x ][ pb(y+2,lenY) ].rho -nd[ pb(x+1,lenX) ][ pb(y+2,lenY) ].rho
				-8.0*nd[ pb(x-1,lenX) ][ pb(y+1,lenY) ].rho +40.0*nd[ x ][ pb(y+1,lenY) ].rho -8.0*nd[ pb(x+1,lenX) ][ pb(y+1,lenY) ].rho 
				-3.0*nd[ pb(x-1,lenX) ][ y ].rho -12.0*nd[ x ][ y ].rho	-3.0*nd[ pb(x+1,lenX) ][ y ].rho)/12.0;
	} else {		
		a = (	-3.0*nd[ x ][ pb(y+1,lenY) ].rho -8.0*nd[ pb(x+1,lenX) ][ pb(y+1,lenY) ].rho -nd[ pb(x+2,lenX) ][ pb(y+1,lenY) ].rho 
				-12.0*nd[ x ][ y ].rho + 40.0*nd[ pb(x+1,lenX) ][ y ].rho -4.0*nd[ pb(x+2,lenX) ][ y ].rho 
				-3.0*nd[ x ][ pb(y-1,lenY) ].rho -8.0*nd[ pb(x+1,lenX) ][ pb(y-1,lenY) ].rho -nd[ pb(x+2,lenX) ][ pb(y-1,lenY) ].rho	)/12.0;
	}
	return a;
}
*/

// mixed difference:
/*
double lattice::derivBC(int x, int y, bool dir){
    double a = derivB(x,y,dir);
    double b = derivC(x,y,dir);
    if( a * b < 0.0 ){
        return b;
    }
    return a;
}
*/

/*
// mixed difference:
double lattice::derivXXBC(int x, int y, bool dir){
    double a = derivXXB(x,y,dir);
    double b = derivXXC(x,y,dir);
    if( a * b < 0.0 ){
        return b;
    }
    return a;
}
*/

/*
//pre stream collision needs an interim array in order to stream correctly
void lattice::preStrColl(double ampl){
    for(int i=0; i<lenX; i++){
        for(int j =0; j<lenY; j++){
            
            // i could still exclude the zeroth node and develop it independently:
            // but not direction 0 though
            
            nd[i][j].g[0] = nd[i][j].g[l] * (1.0 - 1.0/(2.0*relaxTime)) + nd[i][j].gEq[l]/(2.0*relaxTime) 
                                - 0.5*( nd[i][j].u[0]*deriv2M(i,j,0) + nd[i][j].u[1]*deriv2M(i,j,1) )*(nd[i][j].cptGam[0] - wght[0]) 
                                +  ;
            nd[i][j].f[0] = 
            
            for(int l=0; l<9; l++){
                nd[i][j].gBar[l] = nd[i][j].g[l] * (1.0 - 1.0/(2.0*tau)) + nd[i][j].gEq[l]/(2.0*tau) 
                        + ((e[l][0]- nd[i][j].u[0]) *nd[i][j].drv[4] + (e[l][1] - nd[i][j].u[1]) * nd[i][j].drv[5])* (gam-wght[l])/2.0
                        + 3.0/2.0*ampl*( (e[l][0] - nd[i][j].u[0])*( nd[i][j].drv[5]*nd[i][j].drv[8] - nd[i][j].drv[4]*nd[i][j].drv[7]) 
                            + (e[l][1] - nd[i][j].u[1])*( nd[i][j].drv[4]*nd[i][j].drv[8] - nd[i][j].drv[5]*nd[i][j].drv[6]) ) *gam;

                nd[i][j].fBar[l] = nd[i][j].f[l] * (1.0 - 1.0/(2.0*tau)) + gam * nd[i][j].rho /(2.0*tau)
                        + 3.0/2.0*( (e[l][0] -nd[i][j].u[0])*(nd[i][j].drv[4]/3.0 - nd[i][j].rho * nd[i][j].drv[14] ) 
                            + (e[l][1] -nd[i][j].u[1])*(nd[i][j].drv[5]/3.0 - nd[i][j].rho * nd[i][j].drv[15] ) )*gam;  
            }
        }
    }
}
*/


/*
void lattice::postStrColl(double ampl){
    for(int i=0; i<lenX; i++){
        for(int j =0; j<lenY; j++){
            for(int l=0; l<9; l++){
                nd[i][j].g[l] = nd[i][j].g[l] * (1.0 - 1.0/(2.0*tau+1)) + nd[i][j].gEq[l]/(2.0*tau+1) 
                        + ((e[l][0]- nd[i][j].u[0]) * nd[i][j].drv[0] 
                            + (e[l][1] - nd[i][j].u[1]) * nd[i][j].drv[1])* (gam-wght[l])*tau/(2.0*tau +1)
                        + 3.0*tau/(2.0*tau +1)*ampl*((e[l][0]-nd[i][j].u[0])*(nd[i][j].drv[1]*nd[i][j].drv[8]
                            -nd[i][j].drv[0]*nd[i][j].drv[7]) 
                            + (e[l][1] - nd[i][j].u[1])*(nd[i][j].drv[0]*nd[i][j].drv[8] - nd[i][j].drv[1]*nd[i][j].drv[6]) ) *gam;

                nd[i][j].f[l] = nd[i][j].f[l] * (1.0 - 1.0/(2.0*tau+1)) + gam * nd[i][j].rho/(2.0*tau +1.0)
                        + tau/(2.0*tau +1.0)*3.0*( (e[l][0] -nd[i][j].u[0])*(nd[i][j].drv[0]/3.0 - nd[i][j].rho * nd[i][j].drv[10]) 
                            + (e[l][1] -nd[i][j].u[1])*(nd[i][j].drv[1]/3.0 - nd[i][j].rho*nd[i][j].drv[11] ) )*gam; 
            }
        }
    }
}
*/



