// latticeNode.cpp

#include "latticeNode.h"
#include <iostream>
#include <cstdio>

latticeNode::latticeNode(){
	x = -1;
	y = -1;
	u = new double[2];
	u[0] = 0.0;
	u[1] = 0.0;
	g = new double[9];
    gBar = new double[9];
    f = new double[9];
    fBar = new double[9];
    rho = 0.0;
    prssr = 1.0;
    dirDerivRho = new double[9];
    dirDerivB = new double[9];
    dirDerivPsi = new double[9];
    drv = new double[6];
    nn = new int[8];


}

void latticeNode::dump(){
    printf("dump node:\t%d,%d\n",x,y);
    //double sumGEq = 0.0;
    //double sumFEq = 0.0;
    double sumG = 0.0;
    double sumF = 0.0;
    double sumGBar = 0.0;
    double sumFBar = 0.0;

    
    
    for (int i=0; i<9; i++) {
        sumGBar += gBar[i];
    }

    for (int i=0; i<9; i++) {
        sumFBar += fBar[i];
    }

    
    
    for (int i=0; i<9; i++) {
        sumG += g[i];
    }
    
    for (int i=0; i<9; i++) {
        sumF += f[i];
    }
    //rho
    printf("\t\t\trho = %.2f\n",rho);

    //pressure
    printf("\t\t\tpressure = %.2f\n",prssr);

    //u
    printf("\t\t\tu = {");
    for(int i = 0; i<2; i++){
        printf("%.8f  ",u[i]);
    }
    printf("}\n");

    //gBar
    printf("\t\t\tgBar = { ");
    for(int i = 0; i<9; i++){
        printf("%.2f  ",gBar[i]);
    }
    printf("}\t\t\tsum = %f\n",sumGBar);


    //g
    printf("\t\t\tg = { ");
    for(int i = 0; i<9; i++){
        printf("%.2f  ",g[i]);
    }
    printf("}\t\t\tsum = %f\n",sumG);

    //fBar
    printf("\t\t\tfBar = { ");
    for(int i = 0; i<9; i++){
        printf("%.2f  ",fBar[i]);
    }
    printf("}\t\t\tsum = %f\n",sumFBar);

    //f
    printf("\t\t\tf = {");
    for (int i = 0; i<9; i++) {
        printf("%.2f  ",f[i]);
    }
    printf("}\t\t\tsum = %f\n",sumF);   

}
