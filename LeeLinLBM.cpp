// LeeLinLBM.cppLeeLinLBM.cpp


#include <iostream>
#include "math.h"
using namespace std;
#include <cstdio>
#include "auxiliary.h"
#include "lattice.h"


int main(){
    //printf("\n\n\n\n\t\t\thallo, ich bin LeeLinLBM.cpp\n\n");
    

// parameters:
    double rhoH = 1000.0;
    double rhoL = 1.0;
    double rhoSatL = rhoH;      // saturation densities
    double rhoSatV = rhoL; 
    double tauL = 0.1;  //0.1
    double tauV = 1.0;  //1.0
    double intWidth = 5.0;      // interface width
    double srfcTens = 0.0001;    
    double beta = 12.0*srfcTens/(p2(p2(rhoSatL - rhoSatV)) * intWidth);   // interaction amplitude
    double k = 1.5*intWidth*srfcTens/p2(rhoSatL - rhoSatV);             // related to magnitude of surface tension


//debugging parameters:
    bool debug = false;
    int observeX = 30;
    int observeY = 10;

// initializing lattice
    int lengthX = 101;
    int lengthY = 101;
    lattice lt(lengthX, lengthY, rhoH,rhoL,k, beta);
    int midX = 50;
    int midY = 50;
    int radius = 25;    
    lt.setBubble(midX,midY,radius,intWidth);
    //lt.setBar(10,intWidth);
    //lt.checkLatticeForErrors();
    /*    
    for(int i=lt.startX-2; i<lt.endX+1; i++){
        //printf("%d,%f\n",j,lt.nd[lt.endX][j].rho);
        lt.nd[i][lt.endY-2].rho = 400.0;
       lt.nd[i][lt.endY-1].rho = 1000.0;
       lt.nd[i][lt.startY].rho = 600.0;
        lt.nd[i][lt.startY+1].rho = 800.0;
    }
    */
    lt.initialiseBoundaryDerivs();

    //printf("initial state:\n");
    //lt.nd[observeX][observeY].dump();    

    // iteration parametes:
    int itStep = 0;
    int itInterval = 1;            // the mean relative error of laplace's law is averaged over so many steps
    int itBreak = itInterval;
    int itStop =   120;

    bool converged = false;           // to test laplaces law
    int convergenceCounter = 0;
    double* convergenceArray = new double[itInterval];

//-----------------iteration start--------------------
    printf("iteration starts with interface width = %f, surface tension = %f, beta = %.10f, k = %.10f\n\n",
        intWidth,srfcTens,beta,k);

    while (itStep < itStop && !converged ){
        //printf("iteration step: %d\n",itStep+1);
        while (itStep < itBreak && itStep < itStop){
	  
            // pre-streaming collision step with mixed/biased difference:
            lt.preStrCollAndStr(rhoH, rhoL, tauL, tauV);
            lt.macroscopic();
	    lt.postStrColl(rhoH, rhoL, tauL, tauV);			

            //lt.pressureDiff(midX,midY,radius,intWidth,srfcTens,convergenceArray,convergenceCounter);
            convergenceCounter =+1;
            itStep += 1;
        
        }

        //converged = lt.checkConvergence(convergenceArray, itInterval, 0.0001);
        convergenceCounter = 0;
        //lt.nd[observeX][observeY].dump();
        //printf("progress:\t%.1f%%\trelative Error = %f\n\n",double(itStep)/itStop*100,lt.relError);
        printf("progress:\t%.1f%%\n",double(itStep)/itStop*100);
        itBreak += itInterval;
        if(debug){ getchar();}
    }
	
    if(converged){
        printf("iteration converged after %d steps with relative error %f\n",itStep,lt.relError);
    } else{
        //printf("iteration not converged after %d steps\n",itStop);
    }
    
for (int i = lt.startX; i<lt.endX; i++){      // printing outer boundary also
    for ( int j=lt.startY ; j<lt.endY ; j++){
        double x=lt.nd[i][j].rho;
        if (x==x) {} else { printf("False %d %d\n",i,j);}
    }
}
    
// save and plot data:
   
    // prints result matrices for gnuplotIntensity, gnuplotCut, matlab. executes gnuplot. 
   // exportResults(1,1,1);

    // intensity map:
	FILE *pf;
    pf = fopen("densityMap.csv","w");
    bool matlab = false;
	
    if(matlab){
        for (int i = 0; i<lt.lenX; i++){   
            for(int j = 0; j<lt.lenY-1; j++){
                fprintf(pf,"%f, ",lt.nd[i][j].rho);
            }
            fprintf(pf,"%f, ",lt.nd[i][lt.lenY-1].rho);
            fprintf(pf,"\n");
        }
    } else {
        for (int i = lt.startX-2; i<lt.endX+2; i++){      // printing outer boundary also
            for ( int j=lt.startY-2 ; j<lt.endY+2 ; j++){
                fprintf(pf,"%d \t %d \t %f \n",j,i, lt.nd[i][j].rho);
            }
        }
        /*
        for (int i = 0; i<lt.lenX; i++){
            for ( int j=0 ; j<lt.lenY ; j++){
                fprintf(pf,"%d \t %d \t %f \n",j,i, lt.nd[i][j].rho);
            }
        }*/
    }
    
    
	fclose(pf);
    printf("densityMap.csv created \n");

    // profile:
    FILE *pf2;
    pf2 = fopen("densityCut.csv","w");
    for ( int j=0 ; j<lt.lenX ; j++){
        fprintf(pf2,"%d \t %f \n",j, lt.nd[j][observeY].rho);
        //fprintf(pf2,"%d \t %f \n",j, lt.nd[j][observeY].prssr);
    }
    fclose(pf2);
    printf("densityCut.csv created \n");


    FILE *gp;
    gp = popen("gnuplot -persist","w");
    fprintf(gp, "set terminal x11 0\n");
    fprintf(gp, "plot \"densityMap.csv\" using 2:1:3 with image\n");

    fprintf(gp, " set terminal x11 1\n");
    fprintf(gp, "set grid\n");
    //fprintf(gp, "plot \"densityCut.csv\" title 'iterations :%d'\n",itStep);
    fclose(gp);


return 0;
}
